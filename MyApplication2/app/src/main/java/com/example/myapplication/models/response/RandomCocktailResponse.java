package com.example.myapplication.models.response;
import com.example.myapplication.models.entities.Random;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RandomCocktailResponse {
    @SerializedName("drinks")

    private List<Random> drinks ;

    public List<Random> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<Random> drinks) {
        this.drinks = drinks;
    }

    @Override
    public String toString() {
        return "RandomReponse{" +
                "drinks=" + drinks +
                '}';
    }
}
