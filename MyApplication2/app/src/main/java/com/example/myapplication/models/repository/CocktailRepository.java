package com.example.myapplication.models.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.models.api.config.ServiceGenerator;
import com.example.myapplication.models.response.CocktailReponse;
import com.example.myapplication.models.response.DetailReponse;
import com.example.myapplication.models.response.Non_AlcoholicResponse;
import com.example.myapplication.models.response.RandomCocktailResponse;
import com.example.myapplication.models.service.CocktailService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CocktailRepository {
    private CocktailService cocktailService;


    public CocktailRepository() {
        cocktailService = ServiceGenerator.createService(CocktailService.class);
    }


    public MutableLiveData<CocktailReponse> getCocktailList() {

        final MutableLiveData<CocktailReponse> liveData = new MutableLiveData<>();


        Call<CocktailReponse> call = cocktailService.getCocktail();


        call.enqueue(new Callback<CocktailReponse>() {

            @Override
            public void onResponse(Call<CocktailReponse> call, Response<CocktailReponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    liveData.setValue(response.body());
                    Log.d("TAG", "onCocktail: " + liveData.getValue().getDrinks().toString());
                }
            }

            @Override
            public void onFailure(Call<CocktailReponse> call, Throwable t) {

            }
        });
        return liveData;

    }

    public MutableLiveData<RandomCocktailResponse> getRandom() {
        final MutableLiveData<RandomCocktailResponse> liveDataRandom = new MutableLiveData<>();


        Call<RandomCocktailResponse> call = cocktailService.getRandom();
        call.enqueue(new Callback<RandomCocktailResponse>() {


            @Override
            public void onResponse(Call<RandomCocktailResponse> call, Response<RandomCocktailResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    liveDataRandom.setValue(response.body());
                    Log.i("TAG", "onRandom: " + liveDataRandom.getValue().getDrinks().toString());
                }
            }

            @Override
            public void onFailure(Call<RandomCocktailResponse> call, Throwable t) {

            }
        });


        return liveDataRandom;
    }


    public MutableLiveData<Non_AlcoholicResponse> getNon() {
        final MutableLiveData<Non_AlcoholicResponse> liveDataNon = new MutableLiveData<>();


        Call<Non_AlcoholicResponse> call = cocktailService.getNonAlcoholic();
        call.enqueue(new Callback<Non_AlcoholicResponse>() {


            @Override
            public void onResponse(Call<Non_AlcoholicResponse> call, Response<Non_AlcoholicResponse> response) {
                if (response.isSuccessful() && response.body() != null) {

                    liveDataNon.setValue(response.body());
                    Log.i("TAG", "onNon: " + liveDataNon.getValue().getDrinks().toString());
                }
            }

            @Override
            public void onFailure(Call<Non_AlcoholicResponse> call, Throwable t) {

            }
        });


        return liveDataNon;
    }


}
