package com.example.myapplication.view.fragment;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.myapplication.OnItemClickedListener;
import com.example.myapplication.R;
import com.example.myapplication.adapter.CocktailAdapter;
import com.example.myapplication.models.response.CocktailReponse;
import com.example.myapplication.view.Main2Activity;
import com.example.myapplication.viewmodels.CocktailViewModels;

import java.util.ArrayList;

public class CocktailFragment extends Fragment implements OnItemClickedListener {
    private CocktailAdapter adapter;
    private CocktailReponse dataSet;
    private RecyclerView recyclerView;
    private CocktailViewModels cocktailViewModels;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<String> indList=new ArrayList<>();
    private OnItemClickedListener listener;


    //
    private EditText search;
    View view;
    public CocktailFragment(){}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.cocktail_fragment,container,false);
        recyclerView = view.findViewById(R.id.cocktail_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.cocktail_refresh);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        getLiveDataFromViewModel();
    }

    private void getLiveDataFromViewModel() {
        cocktailViewModels = ViewModelProviders.of(this).get(CocktailViewModels.class);
        cocktailViewModels.init();
        cocktailViewModels.getLiveData().observe(this, new Observer<CocktailReponse>() {
            @Override
            public void onChanged(CocktailReponse cocktailReponse){
                dataSet = cocktailReponse;
                adapter = new CocktailAdapter(getContext(),dataSet,listener);
                recyclerView.setAdapter(adapter);
                adapter.setOnItemClickedListener(new OnItemClickedListener() {
                    @Override
                    public void OnClicked(int position) {

//                       Toast.makeText(getContext(),dataSet.getDrinks().get(position).getIdDrink()+"",Toast.LENGTH_LONG).show();
//                       MainActivity.replace((AppindItemsList.add(new Ingredient(dataSet.getDrinks().get(position).getStrIngredient3()));CompatActivity) getActivity(),new CocktailDetailFragment(),bundle);
                        Intent intent = new Intent(getActivity(), Main2Activity.class);
                        intent.putExtra("id",dataSet.getDrinks().get(position).getIdDrink());
                        intent.putExtra("pos",position);

                       startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());


                    }
                });

            }
        });
    }


    @Override
    public void OnClicked(int position) {

    }


}
