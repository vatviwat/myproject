package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;

import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.example.myapplication.R;
import com.example.myapplication.view.fragment.ViewPagerAdapter;
import com.example.myapplication.adapter.CocktailAdapter;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity  {
    private CocktailAdapter adapter;
    private TabLayout tabLayout;
    private AppBarLayout appBarLayout;
    private ViewPager viewPager;
    private ViewFlipper viewFlipper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        tabLayout= findViewById(R.id.tabs_home);
        appBarLayout = findViewById(R.id.cocktail_appbar);
        viewPager = findViewById(R.id.cocktail_viewpager);


        viewFlipper = findViewById(R.id.view_flipper);
        int images[] = {R.drawable.headerimage1,R.drawable.headerimage2};
        for(int image:images){
            flipperImage(image);
        }

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(
                getSupportFragmentManager(), tabLayout.getTabCount()
        );
        viewPager.setAdapter(pagerAdapter);
        tabLayout.getTabAt(0).setIcon(R.drawable.home);
        tabLayout.getTabAt(1).setIcon(R.drawable.heart);
        tabLayout.getTabAt(2).setIcon(R.drawable.fruit);
        tabLayout.getTabAt(3).setIcon(R.drawable.search);
//        tabLayout.getTabAt(0).setText("Home");
//        tabLayout.getTabAt(1).setText("Random");
//        tabLayout.getTabAt(2).setText("Non-Alcoholic");
//        tabLayout.getTabAt(3).setText("Search");


        Toolbar toolbar = findViewById(R.id.cocktail_toolbar);
        setSupportActionBar(toolbar);
    }


    public void flipperImage(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);
        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(6000);
        viewFlipper.setAutoStart(true);

        viewFlipper.setInAnimation(this,R.anim.slide_left);
        viewFlipper.setOutAnimation(this,R.anim.slide_right);

    }


}
