package com.example.myapplication.models.entities;

import com.google.gson.annotations.SerializedName;

public class NonAlcoholic {
    @SerializedName("strDrinkThumb")
    private String strDrinkThumb;

    @SerializedName("idDrink")
    private int idDrink;

    @SerializedName("strDrink")
    private String strDrink;


    public NonAlcoholic(){}
    public NonAlcoholic(String strDrinkThumb, int idDrink, String strDrink) {
        this.strDrinkThumb = strDrinkThumb;
        this.idDrink = idDrink;
        this.strDrink = strDrink;
    }

    public String getStrDrinkThumb() {
        return strDrinkThumb;
    }

    public void setStrDrinkThumb(String strDrinkThumb) {
        this.strDrinkThumb = strDrinkThumb;
    }

    public int getIdDrink() {
        return idDrink;
    }

    public void setIdDrink(int idDrink) {
        this.idDrink = idDrink;
    }

    public String getStrDrink() {
        return strDrink;
    }

    public void setStrDrink(String strDrink) {
        this.strDrink = strDrink;
    }

    @Override
    public String toString() {
        return "NonAlcoholic{" +
                "strDrinkThumb='" + strDrinkThumb + '\'' +
                ", idDrink=" + idDrink +
                ", strDrink='" + strDrink + '\'' +
                '}';
    }
}
