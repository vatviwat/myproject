package com.example.myapplication.models.service;

import android.util.Log;

import com.example.myapplication.models.entities.CocktailDetail;
import com.example.myapplication.models.response.DetailReponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface DetailService {

    @GET("api/json/v1/1/lookup.php")
    Call<DetailReponse> getDetailById(@Query("i") String id);

}