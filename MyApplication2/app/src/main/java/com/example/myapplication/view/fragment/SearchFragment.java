package com.example.myapplication.view.fragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.OnItemClickedListener;
import com.example.myapplication.R;
import com.example.myapplication.adapter.CocktailAdapter;
import com.example.myapplication.models.response.CocktailReponse;
import com.example.myapplication.view.Main2Activity;
import com.example.myapplication.viewmodels.CocktailViewModels;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

public class SearchFragment extends Fragment implements OnItemClickedListener {
    private EditText etSearch;
    private Button btSearch;
    private RelativeLayout containerSearch;
    private CocktailReponse dataSet;
    private RecyclerView rcvSearch;
    private CocktailAdapter adapter;



    private CocktailViewModels cocktailViewModels;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View viewFragment = inflater.inflate(R.layout.search_fragment, null);

        etSearch=viewFragment.findViewById(R.id.et_search);
        btSearch=viewFragment.findViewById(R.id.btn_search);
        containerSearch=viewFragment.findViewById(R.id.container_search);

        btSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    UIUtil.hideKeyboard((Activity) getContext());
                    liveViewModel(etSearch.getText().toString());
                
            }
        });

        return viewFragment;
    }
    public void liveViewModel(String category){
        cocktailViewModels= ViewModelProviders.of(getActivity()).get(CocktailViewModels.class);
        cocktailViewModels.searchInit(category);
        getLiveDataFromViewModel();
    }

    private CocktailReponse getLiveDataFromViewModel() {
        cocktailViewModels.getCocktailSearch().observe(this, new Observer<CocktailReponse>() {
            @Override
            public void onChanged(CocktailReponse cocktailReponse) {
                //Toast.makeText(getContext(),dataSet.getDrinks().size(),Toast.LENGTH_LONG).show();
                dataSet = cocktailReponse;
                if (dataSet.getDrinks()==null) {
                    containerSearch.removeAllViews();
                    notFound();

                }else{
                    rcvSearch = new RecyclerView(getContext());
                    adapter = new CocktailAdapter(getContext(), dataSet, SearchFragment.this::OnClicked);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                    rcvSearch.setLayoutManager(layoutManager);
                    rcvSearch.setAdapter(adapter);

                    RelativeLayout.LayoutParams rcvParam = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                    rcvSearch.setLayoutParams(rcvParam);
                    containerSearch.addView(rcvSearch);
                    adapter.setOnItemClickedListener(new OnItemClickedListener() {
                        @Override
                        public void OnClicked(int position) {
                            Intent intent = new Intent(getActivity(), Main2Activity.class);
                            intent.putExtra("id",dataSet.getDrinks().get(position).getIdDrink());
                            intent.putExtra("pos",position);
                            startActivity(intent);
                        }
                    });
                }

            }
        });


        return dataSet;


    }



    public void notFound(){
        TextView notFound = new TextView(getContext());
        notFound.setText("No Results" + "\n" + "for" + " " + "'" + etSearch.getText() + "'");
        notFound.setTypeface(notFound.getTypeface(), Typeface.BOLD);
        notFound.setTextSize(22);
        notFound.setGravity(Gravity.CENTER);
        RelativeLayout.LayoutParams layoutNotFound = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT);
        layoutNotFound.setMargins(0,30,0,0);
        notFound.setLayoutParams(layoutNotFound);
        containerSearch.addView(notFound);
    }
    @Override
    public void OnClicked(int position) {

    }
}
