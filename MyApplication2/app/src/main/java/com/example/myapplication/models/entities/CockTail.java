package com.example.myapplication.models.entities;

import com.google.gson.annotations.SerializedName;

public class CockTail {
    @SerializedName("strDrinkThumb")
    private String strDrinkThumb;

    @SerializedName("idDrink")
    private String idDrink;

    @SerializedName("strDrink")
    private String strDrink;

    public CockTail(){}

    public CockTail(String strDrinkThumb, String idDrink, String strDrink) {
        this.strDrinkThumb = strDrinkThumb;
        this.idDrink = idDrink;
        this.strDrink = strDrink;
    }

    public String getStrDrinkThumb() {
        return strDrinkThumb;
    }

    public void setStrDrinkThumb(String strDrinkThumb) {
        this.strDrinkThumb = strDrinkThumb;
    }

    public String getIdDrink() {
        return idDrink;
    }

    public void setIdDrink(String idDrink) {
        this.idDrink = idDrink;
    }

    public String getStrDrink() {
        return strDrink;
    }

    public void setStrDrink(String strDrink) {
        this.strDrink = strDrink;
    }

    @Override
    public String toString() {
        return "CockTail{" +
                "strDrinkThumb='" + strDrinkThumb + '\'' +
                ", idDrink=" + idDrink +
                ", strDrink='" + strDrink + '\'' +
                '}';
    }
}
