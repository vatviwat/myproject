package com.example.myapplication.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.adapter.DetailAdapter;
import com.example.myapplication.models.entities.Ingredient;
import com.example.myapplication.models.response.DetailReponse;
import com.example.myapplication.viewmodels.CocktailViewModels;

import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class Main2Activity extends AppCompatActivity {
    private RecyclerView recyclerView;

    //private CocktailDetailResponse dataSet;
    private DetailAdapter adapter;
    private DetailReponse dataSet;
    private String id;
    private Ingredient ingredient;
    private Integer position;
    private TextView indTitle;
    private TextView indTV;
    private TextView indMeasure;
    private TextView indInstruction;
    private ImageView imgCocktail;
    private TextView indID;
    private Button btnBack;
    private ProgressBar progressBar;
    private CocktailViewModels cocktailViewModels;
    private ArrayList<String> indItemsList = new ArrayList<>();
    private ArrayList<String> indMeasureList = new ArrayList<>();

    private LinearLayoutManager linearLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        indID = findViewById(R.id.list_detail_id);
        indID.setVisibility(View.INVISIBLE);
        imgCocktail = findViewById(R.id.list_detail_image);
        indTitle = findViewById(R.id.list_detail_title);
        indTV=findViewById(R.id.list_detail_strIngredient);
        indMeasure=findViewById(R.id.list_detail_measure);
        indInstruction = findViewById(R.id.instruction);
        btnBack = findViewById(R.id.back_button);




        Bundle extra = getIntent().getExtras();
        id = extra.getString("id");
        position=extra.getInt("pos");


        cocktailViewModels=ViewModelProviders.of(this).get(CocktailViewModels.class);
        cocktailViewModels.intiCocktailDetail(id);


        getLiveDataFromViewModel();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                startActivity(intent);

            }
        });


    }
//    private void setupRecyclerView() {
//        if (adapter == null) {
//            adapter = new DetailAdapter(this, dataSet);
//            linearLayoutManager = new LinearLayoutManager(this);
//            recyclerView.setLayoutManager(linearLayoutManager);
//            recyclerView.setAdapter(adapter);
//            adapter.notifyDataSetChanged();
//        }
//
//    }
    private void getLiveDataFromViewModel() {
       cocktailViewModels.getLiveDataCocktailDetail().observe(this, new Observer<DetailReponse>() {
           @Override
           public void onChanged(DetailReponse detailReponse) {
               dataSet = detailReponse;
               checkIngredient();

               String ingredText = "";
               for (String strInd : indItemsList) {
                   ingredText = ingredText + strInd + "\n"+"\n";
               }

               //
               checkMeasure();
               String measureText = "";
               for (String strMea : indMeasureList) {
                   measureText = measureText + strMea + "\n" +"\n";
               }

               Glide.with(Main2Activity.this).load(dataSet.getDrinks().get(0).getStrDrinkThumb()).placeholder(R.drawable.startcocktail).into(imgCocktail);
               indTitle.setText(dataSet.getDrinks().get(0).getStrDrink());
               indInstruction.setText(dataSet.getDrinks().get(0).getStrInstructions());
               indTV.setText(ingredText);
               indMeasure.setText(measureText);



               //Toast.makeText(Main2Activity.this, indItemsList.toString(), Toast.LENGTH_LONG).show();
              // Toast.makeText(Main2Activity.this, dataSet.getDrinks().get(0).getStrDrink(), Toast.LENGTH_SHORT).show();
           }
       });


    }
    private void checkIngredient(){
        if (dataSet.getDrinks().get(0).getStrIngredient1() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient1());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient2() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient2());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient3() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient3());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient4() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient4());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient5() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient5());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient6() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient6());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient7() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient7());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient8() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient8());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient9() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient9());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient10() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient10());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient11() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient11());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient12() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient12());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient13() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient13());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient14() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient14());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient15() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient15());
        }
    }
    public void checkMeasure(){
        if (dataSet.getDrinks().get(0).getStrMeasure1() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure1());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure2() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure2());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure3() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure3());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure4() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure4());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure5() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure5());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure6() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure6());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure7() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure7());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure8() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure8());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure9() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure9());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure10() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure10());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure11() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure11());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure12() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure12());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure13() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrMeasure13());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure14() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure14());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure15() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure15());
        }
    }


}


