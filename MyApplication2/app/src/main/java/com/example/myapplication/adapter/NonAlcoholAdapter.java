package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myapplication.OnItemClickedListener;
import com.example.myapplication.R;
import com.example.myapplication.models.response.Non_AlcoholicResponse;

public class NonAlcoholAdapter extends RecyclerView.Adapter<NonAlcoholAdapter.NonAlcoholViewHolder>  {
    private Context context;
    private Non_AlcoholicResponse dataNon;
    private OnItemClickedListener listener;

    public NonAlcoholAdapter(Context context, Non_AlcoholicResponse dataNon, OnItemClickedListener listener) {
        this.context = context;
        this.dataNon = dataNon;
        this.listener = listener;
    }

    public Non_AlcoholicResponse getDataNon() {
        return dataNon;
    }

    public void setDataNon(Non_AlcoholicResponse dataNon) {
        this.dataNon = dataNon;
    }

    @NonNull
    @Override
    public NonAlcoholAdapter.NonAlcoholViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.ordinary_item_layout, parent, false);
        return new NonAlcoholViewHolder(itemView,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull NonAlcoholAdapter.NonAlcoholViewHolder holder, int position) {
        Glide.with(context).load(dataNon.getDrinks().get(position).getStrDrinkThumb()).placeholder(R.drawable.startcocktail).into(holder.imageNon);
        holder.textId.setText(dataNon.getDrinks().get(position).getIdDrink()+"");
        holder.texttitle.setText(dataNon.getDrinks().get(position).getStrDrink());
    }

    @Override
    public int getItemCount() {
        return dataNon.getDrinks().size();
    }

    public class NonAlcoholViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageNon;
        private TextView textId;
        private TextView texttitle;
        public NonAlcoholViewHolder(@NonNull View itemView, OnItemClickedListener listener) {
            super(itemView);
            imageNon = itemView.findViewById(R.id.list_ordinary_image);
            textId = itemView.findViewById(R.id.list_ordinary_id);
            texttitle = itemView.findViewById(R.id.list_ordinary_title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if (position!=RecyclerView.NO_POSITION) {
                        NonAlcoholAdapter.this.listener.OnClicked(position);
                    }
                }
            });
        }
    }
    public void setOnItemClickedListener(OnItemClickedListener listener)
    {
        this.listener=listener;
    }
}
