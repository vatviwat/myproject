package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.models.entities.Ingredient;
import com.example.myapplication.models.response.DetailReponse;

import java.util.ArrayList;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.DetailViewholder> {
    private Context context;
    private Ingredient ingredient;
    private DetailReponse dataSet;
    private ArrayList<Ingredient> indItemsList = new ArrayList<>();

    public DetailAdapter(Context context, DetailReponse dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    @NonNull
    @Override
    public DetailAdapter.DetailViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
//        View itemView = LayoutInflater.from(parent.getContext())
//                .inflate(R.layout.detail_item_layout,parent,false);
        //return new  DetailAdapter.DetailViewholder(itemView);
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull DetailAdapter.DetailViewholder holder, int position) {
        Glide.with(context).load(dataSet.getDrinks().get(position).getStrDrinkThumb()).placeholder(R.drawable.startcocktail).into(holder.imageDetail);
        //holder.textId.setText((dataSet.getDrinks().get(position).getIdDrink()));
        holder.texttitle.setText(dataSet.getDrinks().get(position).getStrDrink());
        holder.textIntstruction.setText(dataSet.getDrinks().get(position).getStrInstructions());
//        if (dataSet.getDrinks().get(position).getStrIngredient1()!=null)
//        {
            indItemsList.add(new Ingredient(dataSet.getDrinks().get(position).getStrIngredient1()));
            indItemsList.add(new Ingredient(dataSet.getDrinks().get(position).getStrIngredient2()));
            indItemsList.add(new Ingredient(dataSet.getDrinks().get(position).getStrIngredient3()));
            indItemsList.add(new Ingredient(dataSet.getDrinks().get(position).getStrIngredient4()));
//            String s="";
//            for (Ingredient obj:indItemsList){
//                    s = obj.toString();
//            }
            Toast.makeText(context,indItemsList.size()+"",Toast.LENGTH_LONG);
//            indItemsList.add(ingredient);
//        }
//
//        Toast.makeText(context,dataSet.getDrinks().get(position).getStrIngredient1(),Toast.LENGTH_LONG).show();
//        holder.textstrIngredient1.setText(ingredient.toString());

    }

    @Override
    public int getItemCount() {
        return dataSet.getDrinks().size();
    }

    private void addIngredient(DetailReponse dataSet,int position){

    }

    public class DetailViewholder extends RecyclerView.ViewHolder {
        private TextView textId;
        private ImageView imageDetail;
        private TextView texttitle;
        private TextView textIntstruction;
        private TextView textstrIngredient1;
        private TextView textstrIngredient2;
        public DetailViewholder(@NonNull View itemView) {
            super(itemView);
            imageDetail = itemView.findViewById(R.id.list_detail_image);
            textId = itemView.findViewById(R.id.list_detail_id);
            texttitle = itemView.findViewById(R.id.list_detail_title);
           // textIntstruction = itemView.findViewById(R.id.list_detail_instruction);
            //textstrIngredient1 = itemView.findViewById(R.id.list_detail_strIngredient1);
//            textstrIngredient2 = itemView.findViewById(R.id.list_detail_strIngredient2);
        }
    }
}
