package com.example.myapplication.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.myapplication.OnItemClickedListener;
import com.example.myapplication.R;
import com.example.myapplication.adapter.NonAlcoholAdapter;
import com.example.myapplication.models.response.Non_AlcoholicResponse;
import com.example.myapplication.view.Main2Activity;
import com.example.myapplication.viewmodels.CocktailViewModels;

public class OrdinaryFragment extends Fragment implements OnItemClickedListener {
    private CocktailViewModels cocktailViewModels;
    private NonAlcoholAdapter adapter;
    private Non_AlcoholicResponse dataSetNon;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View view;

    //
    private OnItemClickedListener listener;
    public OrdinaryFragment(){}
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ordinary_drink,container,false);
        recyclerView = view.findViewById(R.id.ordinary_recyclerview);
        swipeRefreshLayout = view.findViewById(R.id.ordinary_refresh);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        getLiveDataFromViewModel();
    }

    private void getLiveDataFromViewModel() {

        cocktailViewModels = ViewModelProviders.of(this).get(CocktailViewModels.class);
        cocktailViewModels.initNon();
        cocktailViewModels.getLiveDataNon().observe(this, new Observer<Non_AlcoholicResponse>() {
            @Override
            public void onChanged(Non_AlcoholicResponse non_alcoholicResponse) {
                dataSetNon = non_alcoholicResponse;
                adapter = new NonAlcoholAdapter(getContext(),dataSetNon,listener);
                recyclerView.setAdapter(adapter);
                adapter.setOnItemClickedListener(new OnItemClickedListener() {
                    @Override
                    public void OnClicked(int position) {
                        Intent intent = new Intent(getActivity(), Main2Activity.class);
                        intent.putExtra("id",dataSetNon.getDrinks().get(position).getIdDrink()+"");
                        intent.putExtra("pos",position);
                        startActivity(intent);
//                        Toast.makeText(getContext(),dataSetNon.getDrinks().get(position).getIdDrink()+"",Toast.LENGTH_LONG).show();
//                        Toast.makeText(getContext(),"clicked",Toast.LENGTH_LONG).show();
                    }
                });

            }


        });
    }

    @Override
    public void OnClicked(int position) {

    }
}
