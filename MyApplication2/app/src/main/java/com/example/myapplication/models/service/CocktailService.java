package com.example.myapplication.models.service;

import com.example.myapplication.models.response.CocktailReponse;
import com.example.myapplication.models.response.Non_AlcoholicResponse;
import com.example.myapplication.models.response.RandomCocktailResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CocktailService {
    @GET("https://www.thecocktaildb.com/api/json/v1/1/filter.php?c=Cocktail")
    Call<CocktailReponse> getCocktail();

    @GET("https://www.thecocktaildb.com/api/json/v1/1/random.php")
    Call<RandomCocktailResponse> getRandom();


    @GET("https://www.thecocktaildb.com/api/json/v1/1/filter.php?a=Non_Alcoholic")
    Call<Non_AlcoholicResponse> getNonAlcoholic();


    //detail

//    @GET("api/json/v1/1/lookup.php?i={id}")
//    Call<CocktailDetailResponse> getCocktailDetail(@Path("id")String id);

}
