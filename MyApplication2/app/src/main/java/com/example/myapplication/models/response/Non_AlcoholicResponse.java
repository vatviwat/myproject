package com.example.myapplication.models.response;

import com.example.myapplication.models.entities.NonAlcoholic;
import com.example.myapplication.models.entities.Random;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Non_AlcoholicResponse {
    @SerializedName("drinks")

    private List<NonAlcoholic> drinks ;

    public List<NonAlcoholic> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<NonAlcoholic> drinks) {
        this.drinks = drinks;
    }

    @Override
    public String toString() {
        return "NonResponse{" +
                "drinks=" + drinks +
                '}';
    }
}
