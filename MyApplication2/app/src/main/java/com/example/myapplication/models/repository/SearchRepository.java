package com.example.myapplication.models.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.models.api.config.ServiceGenerator;
import com.example.myapplication.models.response.CocktailReponse;
import com.example.myapplication.models.service.SearchService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchRepository {
    private SearchService searchService;
    public SearchRepository(){
        searchService = ServiceGenerator.createService(SearchService.class);
    }
    public MutableLiveData<CocktailReponse> getCocktailSearch(String s){
        final MutableLiveData<CocktailReponse> liveData = new MutableLiveData<>();
        Call<CocktailReponse> cocktailReponseCall = searchService.getSearchResults(s);
        cocktailReponseCall.enqueue(new Callback<CocktailReponse>() {
            @Override
            public void onResponse(Call<CocktailReponse> call, Response<CocktailReponse> response) {
               if (response.body()!=null){
                   liveData.setValue(response.body());
               }
            }

            @Override
            public void onFailure(Call<CocktailReponse> call, Throwable t) {

            }
        });


        return liveData;
    }
}
