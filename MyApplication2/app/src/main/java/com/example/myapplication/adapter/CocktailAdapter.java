package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import com.example.myapplication.OnItemClickedListener;
import com.example.myapplication.R;
import com.example.myapplication.models.entities.CockTail;
import com.example.myapplication.models.response.CocktailReponse;
import com.example.myapplication.models.response.RandomCocktailResponse;
import com.example.myapplication.viewmodels.CocktailViewModels;

import java.util.ArrayList;
import java.util.List;

public class CocktailAdapter extends RecyclerView.Adapter<CocktailAdapter.CocktailViewHolder>{
    //
    //
    private Context context;
    private CocktailReponse dataSet;
    private OnItemClickedListener listener;




    public CocktailAdapter(Context context, CocktailReponse dataSet, OnItemClickedListener listener) {
        this.context = context;
        this.dataSet = dataSet;
        this.listener = listener;

    }

    public CocktailReponse getDataSet() {
        return dataSet;
    }

    public void setDataSet(CocktailReponse dataSet) {
        this.dataSet = dataSet;
    }

    @NonNull
    @Override
    public CocktailAdapter.CocktailViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.cocktail_item_layout, parent, false);
        return new CocktailViewHolder(itemView,listener);

    }

    @Override
    public void onBindViewHolder(@NonNull CocktailAdapter.CocktailViewHolder holder, int position) {
        Glide.with(context).load(dataSet.getDrinks().get(position).getStrDrinkThumb()).placeholder(R.drawable.startcocktail).into(holder.imageCocktail);
        holder.textId.setText(dataSet.getDrinks().get(position).getIdDrink()+"");
        holder.texttitle.setText(dataSet.getDrinks().get(position).getStrDrink());
    }

    @Override
    public int getItemCount() {
        return dataSet.getDrinks().size();
    }





    public class CocktailViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageCocktail;
        private TextView textId;
        private TextView texttitle;
        public CocktailViewHolder(@NonNull View itemView, OnItemClickedListener listener) {
            super(itemView);
            imageCocktail = itemView.findViewById(R.id.list_cocktail_image);
            textId = itemView.findViewById(R.id.list_cocktail_id);
            texttitle = itemView.findViewById(R.id.list_cocktail_title);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position=getAdapterPosition();
                    if (position!=RecyclerView.NO_POSITION) {
                        listener.OnClicked(position);
                    }
                }
            });
        }

    }
    public void setOnItemClickedListener(OnItemClickedListener listener)
    {
        this.listener=listener;
    }
}
