package com.example.myapplication.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.models.response.RandomCocktailResponse;

import java.util.ArrayList;

public class RandomAdapter extends RecyclerView.Adapter<RandomAdapter.RandomViewHolder> {
    private Context context;
    private RandomCocktailResponse dataSet;
    private ArrayList<String> indItemsList = new ArrayList<>();
    private ArrayList<String> indMeasureList = new ArrayList<>();

    public RandomAdapter(Context context, RandomCocktailResponse dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    public RandomCocktailResponse getDataSet() {
        return dataSet;
    }

    public void setDataSet(RandomCocktailResponse dataSet) {
        this.dataSet = dataSet;
    }

    @NonNull
    @Override
    public RandomAdapter.RandomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.random_item_layout, parent, false);
        return new RandomAdapter.RandomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RandomAdapter.RandomViewHolder holder, int position) {
        Glide.with(context).load(dataSet.getDrinks().get(position).getStrDrinkThumb()).placeholder(R.drawable.startcocktail).into(holder.imageRandom);
//        holder.textId.setText(dataSet.getDrinks().get(position).getIdDrink()+"");
        holder.texttitle.setText(dataSet.getDrinks().get(position).getStrDrink());
        checkIngredient();

        String ingredText = "";
        for (String strInd : indItemsList) {
            ingredText = ingredText + strInd + "\n";
        }
        holder.textFirst.setText(ingredText);


        checkMeasure();
        String measureText = "";
        for (String strMea : indMeasureList) {
            measureText = measureText + strMea + "\n";
        }
        holder.textSecond.setText(measureText);
        holder.textInstruction.setText(dataSet.getDrinks().get(position).getStrInstructions());
    }

    @Override
    public int getItemCount() {
        return dataSet.getDrinks().size();
    }

    public class RandomViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageRandom;
        private TextView textId;
        private TextView texttitle;
        private TextView textFirst;
        private TextView textSecond;
        private TextView textInstruction;
        public RandomViewHolder(@NonNull View itemView) {
            super(itemView);
            imageRandom = itemView.findViewById(R.id.list_random_image);
           // textId = itemView.findViewById(R.id.list_random_id);
            texttitle = itemView.findViewById(R.id.list_random_title);
            textFirst = itemView.findViewById(R.id.first);
            textSecond = itemView.findViewById(R.id.measure);
            textInstruction = itemView.findViewById(R.id.text_instruction);
        }
    }
    private void checkIngredient(){
        if (dataSet.getDrinks().get(0).getStrIngredient1() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient1());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient2() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient2());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient3() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient3());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient4() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient4());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient5() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient5());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient6() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient6());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient7() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient7());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient8() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient8());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient9() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient9());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient10() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient10());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient11() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient11());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient12() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient12());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient13() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient13());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient14() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient14());
        }
        if (dataSet.getDrinks().get(0).getStrIngredient15() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrIngredient15());
        }
    }
    public void checkMeasure(){
        if (dataSet.getDrinks().get(0).getStrMeasure1() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure1());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure2() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure2());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure3() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure3());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure4() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure4());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure5() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure5());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure6() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure6());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure7() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure7());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure8() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure8());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure9() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure9());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure10() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure10());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure11() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure11());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure12() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure12());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure13() != null) {
            indItemsList.add(dataSet.getDrinks().get(0).getStrMeasure13());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure14() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure14());
        }
        if (dataSet.getDrinks().get(0).getStrMeasure15() != null) {
            indMeasureList.add(dataSet.getDrinks().get(0).getStrMeasure15());
        }
    }
}
