package com.example.myapplication.models.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.myapplication.models.api.config.ServiceGenerator;
import com.example.myapplication.models.entities.CocktailDetail;
import com.example.myapplication.models.response.DetailReponse;
import com.example.myapplication.models.service.DetailService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CocktailDetailRepository {
    private DetailService detailService;

    public CocktailDetailRepository() {
         detailService = ServiceGenerator.createService(DetailService.class);
    }

    public MutableLiveData<DetailReponse> getCocktailDetail(String id) {
        final MutableLiveData<DetailReponse> liveData = new MutableLiveData<>();
        Log.d("id", id);
        Integer i=Integer.parseInt(id);
        Call<DetailReponse> detailResponseCall = detailService.getDetailById(id);
        detailResponseCall.enqueue(new Callback<DetailReponse>() {
            @Override
            public void onResponse(Call<DetailReponse> call, Response<DetailReponse> response) {
                liveData.setValue(response.body());
                Log.d("123","onresponse:"+response.body());
            }

            @Override
            public void onFailure(Call<DetailReponse> call, Throwable t) {
                Log.d("ERROR", "onFailure: "+t.getMessage());

            }
        });
        return liveData;
    }
}
