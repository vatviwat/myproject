package com.example.myapplication.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.myapplication.view.fragment.CocktailFragment;
import com.example.myapplication.view.fragment.OrdinaryFragment;
import com.example.myapplication.view.fragment.RandomFragment;

public class ViewPagerAdapter  extends FragmentPagerAdapter {
    private int tabCount;

    public ViewPagerAdapter(@NonNull FragmentManager fm, int tabCount) {
        super(fm, tabCount);
        this.tabCount = tabCount;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new CocktailFragment();break;
            case 1:
                fragment = new RandomFragment(); break;
            case 2:
                fragment = new OrdinaryFragment();break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
