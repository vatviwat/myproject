package com.example.myapplication.models.service;

import com.example.myapplication.models.response.CocktailReponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchService {
    @GET("api/json/v1/1/search.php?")
    Call<CocktailReponse> getSearchResults(@Query("s") String search);
}
