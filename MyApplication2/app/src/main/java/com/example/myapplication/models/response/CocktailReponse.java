package com.example.myapplication.models.response;

import com.example.myapplication.models.entities.CockTail;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CocktailReponse {
    @SerializedName("drinks")

    private List<CockTail> drinks = new ArrayList<>();

    public List<CockTail> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<CockTail> drinks) {
        this.drinks = drinks;
    }

    @Override
    public String toString() {
        return "CocktailReponse{" +
                "drinks=" + drinks +
                '}';
    }
}
