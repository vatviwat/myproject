package com.example.myapplication.view.fragment;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;


public class ViewPagerAdapter  extends FragmentPagerAdapter{
    private int numOfTab;
    private static final String[] title = {"Cocktail","Random","Ordinary","Search"};

    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        numOfTab = behavior;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if(position==0){
            CocktailFragment cocktailFragment = new CocktailFragment();
            return cocktailFragment;
        }else if(position==1){
            RandomFragment randomFragment2 = new RandomFragment();
            return randomFragment2;
        }
        else if(position==2){
            OrdinaryFragment ordinaryFragment= new OrdinaryFragment();
            return ordinaryFragment;
        }
        else if(position==3){
            SearchFragment searchFragment = new SearchFragment();
            return searchFragment;
        }
        else
            return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

}