package com.example.myapplication.models.response;

import com.example.myapplication.models.entities.CocktailDetail;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailReponse {
    @SerializedName("drinks")

    private List<CocktailDetail> drinks ;

    public List<CocktailDetail> getDrinks() {
        return drinks;
    }

    public void setDrinks(List<CocktailDetail> drinks) {
        this.drinks = drinks;
    }

    @Override
    public String toString() {
        return "DetailReponse{" +
                "drinks=" + drinks +
                '}';
    }
}
