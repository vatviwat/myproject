package com.example.myapplication.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.example.myapplication.R;
import com.example.myapplication.adapter.CocktailAdapter;
import com.example.myapplication.adapter.RandomAdapter;
import com.example.myapplication.models.response.CocktailReponse;
import com.example.myapplication.models.response.RandomCocktailResponse;
import com.example.myapplication.viewmodels.CocktailViewModels;

public class RandomFragment extends Fragment {
    private CocktailViewModels cocktailViewModels;
    private RandomAdapter adapter;
    private RandomCocktailResponse dataSetRandom;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View view;
    public RandomFragment(){}
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.random_fragment,container,false);
        recyclerView = view.findViewById(R.id.random_recyclerview);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        getLiveDataFromViewModel();
    }

    private void getLiveDataFromViewModel() {

        cocktailViewModels = ViewModelProviders.of(this).get(CocktailViewModels.class);
        cocktailViewModels.initRandom();
        cocktailViewModels.getLiveDataRandom().observe(this, new Observer<RandomCocktailResponse>() {
            @Override
            public void onChanged(RandomCocktailResponse randomCocktailResponse) {
                dataSetRandom = randomCocktailResponse;
                adapter = new RandomAdapter(getContext(),dataSetRandom);
                recyclerView.setAdapter(adapter);
            }
        });
    }

}
