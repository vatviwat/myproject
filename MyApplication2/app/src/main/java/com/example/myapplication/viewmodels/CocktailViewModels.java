package com.example.myapplication.viewmodels;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplication.models.repository.CocktailDetailRepository;
import com.example.myapplication.models.repository.CocktailRepository;
import com.example.myapplication.models.repository.SearchRepository;
import com.example.myapplication.models.response.CocktailReponse;
import com.example.myapplication.models.response.DetailReponse;
import com.example.myapplication.models.response.Non_AlcoholicResponse;
import com.example.myapplication.models.response.RandomCocktailResponse;

public class CocktailViewModels extends ViewModel {
    private LiveData<CocktailReponse> liveData;
    private LiveData<RandomCocktailResponse> liveDataRandom;
    private LiveData<Non_AlcoholicResponse> liveDataNon;

    private CocktailRepository cocktailRepository, randomRepos;


    private LiveData<DetailReponse> liveDataCocktailDetail;
    private CocktailDetailRepository cocktailDetailRepository;


    private SearchRepository searchRepository;

    public void init() {
        cocktailRepository = new CocktailRepository();
        liveData=cocktailRepository.getCocktailList();
    }

    public LiveData<CocktailReponse> getLiveData() {
        return liveData;
    }


    public void initRandom(){
        randomRepos = new CocktailRepository();
        liveDataRandom = randomRepos.getRandom();
    }
    public LiveData<RandomCocktailResponse> getLiveDataRandom(){
        return liveDataRandom;
    }


    public void initNon(){
        randomRepos = new CocktailRepository();
        liveDataNon= randomRepos.getNon();
    }
    public LiveData<Non_AlcoholicResponse> getLiveDataNon(){
        return liveDataNon;
    }



    public void intiCocktailDetail(String id){
        cocktailDetailRepository = new CocktailDetailRepository();
        liveDataCocktailDetail = cocktailDetailRepository.getCocktailDetail(id);
    }
    public LiveData<DetailReponse> getLiveDataCocktailDetail(){
        return liveDataCocktailDetail;
    }

    public void searchInit(String category) {
        searchRepository = new SearchRepository();
        liveData = searchRepository.getCocktailSearch(category);
    }
    public LiveData<CocktailReponse> getCocktailSearch(){
        return liveData;
    }

}
