package com.example.myapplication.models.entities;

public class Ingredient {

    private String strIngredient;
    private String strMeasure;

    public Ingredient(String strIngredient) {
        this.strIngredient = strIngredient;
    }

    public String getStrIngredient() {
        return strIngredient;
    }

    public void setStrIngredient(String strIngredient) {
        this.strIngredient = strIngredient;
    }

    public String getStrMeasure() {
        return strMeasure;
    }

    public void setStrMeasure(String strMeasure) {
        this.strMeasure = strMeasure;
    }

    @Override
    public String toString() {
        return strIngredient + ' ';
    }
}
