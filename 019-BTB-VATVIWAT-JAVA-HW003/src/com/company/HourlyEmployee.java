package com.company;

public class HourlyEmployee extends StaffMember {

    private int hoursWorked;
    private double rate;

    public HourlyEmployee(int id, String name, String address, String isMember, int hoursWorked, double rate) {
        super(id, name, address, isMember);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }

    public HourlyEmployee(){}

    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return  "id = " + id + "\n" + "name = " + name + "\n" + "address = " + address + "\n" + "hoursWorked = " +
                hoursWorked + "\n" + "rate = " + rate + "\n" + "Payment = " + pay() + "\n";
    }
    @Override
    public double pay() {
        if(getRate()<0||Double.isNaN(getRate())){
            return Double.NaN;
        }
        else if(getHoursWorked()<0){
            return Double.NaN;
        }
        else{
            return getHoursWorked()*getRate();
        }
    }
}
