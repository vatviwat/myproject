package com.company;
public class SalariedEmployee extends StaffMember{
    private double salary;
    private double bonus;

    public SalariedEmployee(int id, String name, String address, String isMember, double salary, double bonus) {
        super(id, name, address, isMember);
        this.salary = salary;
        this.bonus = bonus;
    }

    public SalariedEmployee(){}

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    @Override
    public String toString() {
        return  "id = " + id + "\n" + "name = " + name + "\n" + "address = " + address + "\n" + "salary = " +
                salary + "\n" + "bonus = " + bonus + "\n" + "Payment = " + pay() + "\n";
    }

    @Override
    public double pay() {
        if(getSalary()<0||Double.isNaN(getSalary())){
            return Double.NaN;
        }
        else if(getBonus()<0||Double.isNaN(getBonus())){
            return  Double.NaN;
        }
        else {
            return getSalary() + getBonus();
        }
    }
}
