package com.company;
import java.util.*;
public class Main {
    ArrayList<StaffMember> staffMemberArrayList = new ArrayList<>();
    Volunteer volunteer = new Volunteer();
    SalariedEmployee salariedEmployee = new SalariedEmployee();
    HourlyEmployee hourlyEmployee = new HourlyEmployee();
    String isMember;
    static Scanner input = new Scanner(System.in);
    public static void main(String[] args){
        Main m = new Main();
        m.initialize();
        while(true) {
            m.display();
            System.out.println("========= MENU =========");
            System.out.println("1.) Add Employee");
            System.out.println("2.) Edit");
            System.out.println("3.) Remove");
            System.out.println("4.) Exit");
            String opt;
            Scanner input = new Scanner(System.in);
            System.out.print("=> Choose option(1-4): ");
            opt = input.next();
            outer: switch (opt){
                case "1":   System.out.println("========= ADD EMPLOYEE =========");
                            System.out.println("1.) Add Volunteer");
                            System.out.println("2.) Add Salaried Employee");
                            System.out.println("3.) Add Hourly Employee");
                            System.out.println("4.) Back");
                            String opt2;
                            System.out.print("=> Choose option(1-4): ");
                            opt2 = input.next();
                            switch (opt2){
                                case "1":   m.addVolunteer(); break;
                                case "2":   m.addSalariedEmployee(); break;
                                case "3":   m.addHourlyEmployee(); break;
                                case "4":   break outer;
                                default:    System.out.println("\nPlease Input Invalid Number...\n");
                            }
                            break;
                case "2":   m.editEmployee(); break;
                case "3":   m.removeEmployee(); break;
                case "4":   System.out.println("~Good Bye~");
                            System.exit(0);
                default:    System.out.println("\nPlease Input Invalid Number...\n");
            }
        }
    }

    /** init */
    public void initialize(){
        staffMemberArrayList.add(new HourlyEmployee(3,"vanda","PP","H",10,50));
        staffMemberArrayList.add(new SalariedEmployee(2,"senglong","PP","S",Double.NaN,-100));
        staffMemberArrayList.add(new Volunteer(1,"viwat","PP","V"));
        staffMemberArrayList.add(new Volunteer(4,"udom","PP","V"));
    }

    /** add Volunteer */
    public void addVolunteer(){
        System.out.println("========= INSERT VOLUNTEER INFORMATION =========");
        System.out.print("=> Enter Member Staff's ID: ");
        int id = inputInt();
        input.nextLine();
        System.out.print("=> Enter Member Staff's Name   : ");
        String name = inputString();
        System.out.print("=> Enter Member Staff's Address: ");
        String address = inputString();
        isMember = "V";
        boolean flag = false;
        for (StaffMember staffMember : staffMemberArrayList) {
            if (staffMember.getId() == id) {
                flag = true;
                System.out.println("\nDuplicate ID... Please Input Again!\n");
            }
        }
        if (!flag) {
            volunteer = new Volunteer(id, name, address, isMember);
            staffMemberArrayList.add(volunteer);
        }
    }

    /** add SalariedEmployee */
    public void addSalariedEmployee(){
        System.out.println("========= INSERT SALARIED EMPLOYEE INFORMATION =========");
        System.out.print("=> Enter Member Staff's ID: ");
        int id = inputInt();
        input.nextLine();
        System.out.print("=> Enter Member Staff's Name   : ");
        String name = inputString();
        System.out.print("=> Enter Member Staff's Address: ");
        String address = inputString();
        System.out.print("=> Enter Member Staff's Salary : ");
        double salary = inputDouble();
        System.out.print("=> Enter Member Staff's Bonus  : ");
        double bonus = inputDouble();
        isMember = "S";
        boolean flag = false;
        for (StaffMember staffMember : staffMemberArrayList) {
            if (staffMember.getId() == id) {
                flag = true;
                System.out.println("\nDuplicate ID... Please Input Again!\n");
            }
        }
        if (!flag) {
            salariedEmployee = new SalariedEmployee(id, name, address, isMember, salary, bonus);
            staffMemberArrayList.add(salariedEmployee);
        }
    }

    /** add HourlyEmployee */
    public void addHourlyEmployee(){
        System.out.println("========= INSERT HOURLY EMPLOYEE INFORMATION =========");
        System.out.print("=> Enter Member Staff's ID: ");
        int id = inputInt();
        input.nextLine();
        System.out.print("=> Enter Member Staff's Name: ");
        String name = inputString();
        System.out.print("=> Enter Member Staff's Address: ");
        String address = inputString();
        System.out.print("=> Enter Member Staff's HoursWorked: ");
        int hour = inputInt();
        System.out.print("=> Enter Member Staff's Rate: ");
        double rate = inputDouble();
        isMember = "H";
        boolean flag = false;
        for (StaffMember staffMember : staffMemberArrayList) {
            if (staffMember.getId() == id) {
                flag = true;
                System.out.println("\nDuplicate ID... Please Input Again!\n");
            }
        }
        if (!flag) {
            hourlyEmployee = new HourlyEmployee(id, name, address, isMember, hour, rate);
            staffMemberArrayList.add(hourlyEmployee);
        }
    }

    /** display with sort */
    public void display() {
        System.out.println("========= STAFF INFORMATION =========");
        boolean flag = false;
        staffMemberArrayList.sort(Main.sortList);
        for (StaffMember staffMember : staffMemberArrayList) {
            if(staffMember!=null) {
                flag = true;
                System.out.println();
                System.out.println(staffMember);
                System.out.println("----------------------------------------");
            }
        }
        if(!flag){
            System.out.println("\nNo Information\n");
        }
    }

    /** sort method */
    public static Comparator<StaffMember> sortList = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember s1, StaffMember s2) {
            String Name1 = s1.getName().toUpperCase();
            String Name2 = s2.getName().toUpperCase();
            return Name1.compareTo(Name2);
        }
    };
    /** edit */
    public void editEmployee () {
        System.out.println("========= EDIT STAFF INFORMATION =========");
        System.out.print("=> Enter Staff Member ID's to Update: ");
        int d = inputInt();
        boolean flag = false;
        boolean flag2 = false;
        boolean flag3 = false;
        for (StaffMember staffMember : staffMemberArrayList) {
            if (staffMember.getId() == d && staffMember.getIsMember() == "V") {
                flag = true;
                System.out.print(staffMember);
                System.out.println("========= NEW INFORMATION OF STAFF MEMBER =========");
                input.nextLine();
                System.out.print("=> Enter Staff Member's Name   : ");
                String name = inputString();
                System.out.print("=> Enter Staff Member's Address: ");
                String address = inputString();
                volunteer = new Volunteer(d, name, address, volunteer.getIsMember());
                int index = staffMemberArrayList.indexOf(staffMember);
                staffMemberArrayList.set(index, volunteer);
            } else if (staffMember.getId() == d && staffMember.getIsMember() == "S") {
                flag2 = true;
                System.out.print(staffMember);
                System.out.println("========= NEW INFORMATION OF STAFF MEMBER =========");
                input.nextLine();
                System.out.print("=> Enter Staff Member's Name  : ");
                String name = inputString();
                System.out.print("=> Enter Staff Member's Salary: ");
                double salary = inputDouble();
                System.out.print("=> Enter Staff Member's Bonus : ");
                double bonus = inputDouble();
                salariedEmployee = new SalariedEmployee(d, name, staffMember.getAddress(), staffMember.getIsMember(), salary, bonus);
                int index = staffMemberArrayList.indexOf(staffMember);
                staffMemberArrayList.set(index, salariedEmployee);

            } else if (staffMember.getId() == d && staffMember.getIsMember() == "H") {
                flag3 = true;
                System.out.print(staffMember);
                System.out.println("========= NEW INFORMATION OF STAFF MEMBER =========");
                input.nextLine();
                System.out.print("=> Enter Staff Member's Name: ");
                String name = inputString();
                System.out.print("=> Enter Staff Member's HourlyWorked: ");
                int hours = inputInt();
                System.out.print("=> Enter Staff Member's Rate: ");
                double rate = inputDouble();
                hourlyEmployee = new HourlyEmployee(d, name, staffMember.getAddress(), staffMember.getIsMember(), hours, rate);
                int index = staffMemberArrayList.indexOf(staffMember);
                staffMemberArrayList.set(index, hourlyEmployee);
            }
        }
        if (!flag) {
            if (!flag2) {
                if (!flag3) {
                    System.out.println("\n-> Member Staff's ID Not Found...\n");
                }
            }
        }
    }
    /** remove */
    public void removeEmployee() {
        try {
            System.out.println("========= DELETE STAFF INFORMATION =========");
            System.out.print("=> Enter Member Staff's ID To Remove: ");
            int d = inputInt();
            boolean found = false;
            for (StaffMember staffMember : staffMemberArrayList) {
                if (staffMember.getId() == d) {
                    found = true;
                    staffMemberArrayList.remove(staffMember);
                    System.out.print(staffMember);
                    System.out.println("This Information has been Removed Successfully\n");
                }
            }
            if (!found) {
                System.out.println("\n-> Member Staff's ID Not Found...\n");
            }
        }catch (ConcurrentModificationException e){ }
    }
    /** validate method */
    static int inputInt() {
        int baseNum;
        while(true) {
            while(!input.hasNextInt()) {
                if(!input.hasNextInt()) {
                    input.next();
                    System.out.print("\tInvalid input... Please Input Again: ");
                }
            }
            baseNum=input.nextInt();
            if(baseNum < 0) {
                System.out.print("\tInvalid input... Please Input Again: ");
            }
            else {
                break;
            }
        }
        return baseNum;
    }
    static double inputDouble() {
        double baseNum;
        while(true) {
            while(!input.hasNextDouble()) {
                if(!input.hasNextDouble()) {
                    input.next();
                    System.out.print("\tInvalid input..., Please Input Again: ");
                }
            }
            baseNum=input.nextDouble();
            if(baseNum < 0) {
                System.out.print("\tInvalid input..., Please Input Again: ");
            }
            else {
                break;
            }
        }
        return baseNum;
    }
    static String inputString() {
        boolean isBoolean   ;
        String name;
        do {
            String usernamePattern = "[a-zA-Z ]+";
            name = input.nextLine();
            isBoolean = name.matches(usernamePattern);
            if (!isBoolean) {
                System.out.print("\tPlease input correct name(a-z,A-Z): ");
                isBoolean=false;
            }
        } while (!isBoolean);
        return name;
    }
    /** validate method */
}
