package com.company;

public class Volunteer extends StaffMember {
    public Volunteer(int id, String name, String address, String isMember) {
        super(id, name, address, isMember);
    }
    public Volunteer(){}

    @Override
    public String toString() {
        return  "id = " + id + "\n" + "name = " + name + "\n" + "address = " + address + "\n";
    }
    @Override
    public double pay() {
        return 0;
    }
}
