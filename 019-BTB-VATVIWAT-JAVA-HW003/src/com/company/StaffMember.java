package com.company;

public abstract class StaffMember {
    protected int id;
    protected String name;
    protected String address;
    protected String isMember;

    public StaffMember(int id, String name, String address, String isMember) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.isMember = isMember;
    }
    public StaffMember(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIsMember() {
        return isMember;
    }

    public void setIsMember(String isMember) {
        this.isMember = isMember;
    }

    @Override
    public String toString() {
        return  "id = " + id + "\n" + "name = " + name + "\n" + "address = " + address + "\n";
    }

    public abstract double pay();
}
