import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

const user={
    first_name: 'viwat',
    last_name: 'vat'
};
function formatName(user) {
    return user.last_name + " " + user.first_name;
}

const element = ( 
    <h1 style={{textAlign: "center", background: "green", color: "white"}}>
        {formatName(user)}
    </h1>
 );

ReactDOM.render(
    element,
    document.getElementById('root')
);